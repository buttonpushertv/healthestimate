# If you're unfamiliar with YAML, this comment will explain the syntax you can find in this file:
#    "-" denotes a list item;
#    ">-" and "|-" both allow you to break up text into several lines. The difference being "|-" keeps your linebreaks,
#         while ">-" only adds a linebreak if you have an empty line in the text (see Death Marker entry for example)
#
# Generally you can just replace the text without having to worry about that.
# However using an editor that can highlight the syntax could still be helpful.

healthEstimate:
    core:
        onlyGM:
            name: GM only
            hint: Only show estimations to GM.
        onlyNPCs:
            name: NPCs only
            hint: Only show estimations for NPCs, but not for player characters.
        stateNames:
            name: Stages
            hint: An array of descriptions shown to players, arranged from worst to best possible health.
            default:
                - Unconscious
                - Near Death
                - Badly Injured
                - Injured
                - Barely Injured
                - Uninjured
        deathState:
            name: Death State on Condition
            hint: Show token as dead when it's been marked with specific condition. Works independently from the macro.
        deathStateName:
            name: Death State Name
            hint: The description shown to players when the token is dead. Compendium tab has a macro for marking a token (or several) as dead.
            default: Dead
        deathMarker:
            name: Death Marker
            hint: >-
                Path to the condition icon that marks the dead tokens.

                To get a custom icon's path, select a token marked with it, open the console and enter
                "canvas.tokens.controlled[0].data.overlayEffect"
        addTemp:
            name: Add Temporary Health
            hint: |-
                Include temporary health points in calculating what description to show.
                E.g. with the setting turned on the token receiving temp HP will look "healthier" unless it's already at max health.
        fontSize:
            name: Font Size
            hint: Accepts any valid property for CSS font-size
        color:
            name: Color
            hint: Should the descriptions be highlighted with a gradient from green to red corresponding to current health?
        smoothGradient:
            name: Smooth Gradient
            hint: >-
                With this setting off the color will only change when the next description is reached,
                with it on it will reflect the health percentage.
    PF1:
        showExtra:
            name: Disabled & Dying
            hint: Show "Disabled" and "Dying" descriptions
        disabledName:
            name: Disabled
            hint: Description to show when token is precisely at 0 HP
            default: Disabled
        dyingName:
            name: Dying
            hint: Description to show when token is below 0 HP
            default: Dying
    numenera:
        countPools:
            name: Count Empty Pools
            hint: Count empty pools instead of sum of their values, with the estimation only changing when a pool is depleted/replenished
    starfinder:
        useThreshold:
            name: Use Vehicle Threshold
            hint: If this setting is on, the "Threshold Stages" descriptions will be used for vehicles above threshold, below it, and at 0 HP respectively.
        thresholdNames:
            name: Threshold Stages
            hint: The amount of stages is limited to 3, arranged from worst to best condition
            default:
                - Wrecked
                - Broken
                - Fully Functional
        vehicleNames:
            name: Vehicle Stages
            hint: Descriptions shown for starships (always), and vehicles (if "Use Vehicle Threshold" is off)
            default:        #There are no official descriptions for the states of the ship, so feel free to go with what you feel will work best in your language
                - Wrecked
                - Flaming
                - Smoking
                - Battered
                - Scratched
                - Pristine
        addStamina:
            name: Account for Stamina
            hint: With this setting on Stamina will be added to HP for calculations.
    worldbuilding:
        simpleRule:
            name: Estimation Rule
            hint: >-
                The JS formula for calculating the fraction of health token has. Alter with caution.
                Default assumes simple descending health, e.g. you start with 10/10 in your health field and go down to 0/10 when taking damage.
            default: const hp = token.actor.data.data.health; return hp.value / hp.max
